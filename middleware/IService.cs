﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace AgriBeef.WarehouseMW
{
    [ServiceContract]
    public partial interface IService
    {

        [OperationContract,
         WebGet(UriTemplate = "/echo/{echoString}",
                ResponseFormat = WebMessageFormat.Json)]
        String Echo(String echoString);
    }

}
