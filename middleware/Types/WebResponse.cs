﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AgriBeef.WarehouseMW.Types
{
    public partial class WebResponse
    {
        public WebResponse()
        {
            this.Successful = false;
            this.ErrorId = 0;
            this.Message = "";
        }
        public int ErrorId { get; set; }
        public string Message { get; set; }
        public bool Successful { get; set; }
    }
}