﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AgriBeef.WarehouseMW.Types
{
    public class PickSerialRequest
    {
        public string OrderNum { get; set; }
        public string LicensePlateId { get; set; }
        public string SerialId { get; set; }
    }
}