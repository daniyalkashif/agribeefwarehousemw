﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AgriBeef.WarehouseMW.Types
{
    public partial class WebResponse
    {
        public static implicit operator WebResponse(WHSMobileValidation.abWHSMobileValidationServiceCheckOrderNumResponse axResponse)
        {
            WebResponse webResponse = new WebResponse();

            webResponse.ErrorId = axResponse.response.ErrorId;
            webResponse.Message = axResponse.response.Message;
            webResponse.Successful = Convert.ToBoolean(axResponse.response.Successful);

            return webResponse;
        }

        public static implicit operator WebResponse(WHSMobileValidation.abWHSMobileValidationServiceCheckSerialResponse axResponse)
        {
            WebResponse webResponse = new WebResponse();

            webResponse.ErrorId = axResponse.response.ErrorId;
            webResponse.Message = axResponse.response.Message;
            webResponse.Successful = Convert.ToBoolean(axResponse.response.Successful);

            return webResponse;
        }

        public static implicit operator WebResponse(WHSMobileValidation.abWHSMobileValidationServiceCheckStageLPResponse axResponse)
        {
            WebResponse webResponse = new WebResponse();

            webResponse.ErrorId = axResponse.response.ErrorId;
            webResponse.Message = axResponse.response.Message;
            webResponse.Successful = Convert.ToBoolean(axResponse.response.Successful);

            return webResponse;
        }

        public static implicit operator WebResponse(WHSMobileValidation.abWHSMobileValidationServicePickSerialResponse axResponse)
        {
            WebResponse webResponse = new WebResponse();

            webResponse.ErrorId = axResponse.response.ErrorId;
            webResponse.Message = axResponse.response.Message;
            webResponse.Successful = Convert.ToBoolean(axResponse.response.Successful);

            return webResponse;
        }
    }
}