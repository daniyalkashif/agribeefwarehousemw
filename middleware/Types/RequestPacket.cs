﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AgriBeef.WarehouseMW.Types
{
    public class RequestPacket
    {
        public RequestPacket() { }
        public String RequestUrl { get; set; }
        public String UserAgent { get; set; }
        public String User { get; set; }
        public String Method { get; set; }
        public String QueryParams { get; set; }
        public String RequestBody { get; set; }
        public String ReplyBody { get; set; }
        public DateTime ArrivedAt { get; set; }
        public DateTime DispatchedAt { get; set; }
        public double ProcessedIn
        {
            get
            {
                return DispatchedAt.Subtract(ArrivedAt).TotalSeconds;
            }
        }
    }
}