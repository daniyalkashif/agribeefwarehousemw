﻿using AgriBeef.WarehouseMW.Types;
using AgriBeef.WarehouseMW.Common;
using System.Configuration;
using AgriBeef.WarehouseMW.Common.Constants;

namespace AgriBeef.WarehouseMW
{
    public partial class Service: IService
    {
        public WebResponse CheckOrderNum(string orderNum)
        {
            WebResponse webResponse = new WebResponse();

            Logger.LogInfo(string.Format("OrderNum: {0}", orderNum));

            var provider = Providers.ProviderFactory.GetWarehouseProvider();
            var response = provider.CheckOrderNum(orderNum);

            webResponse = response.Value;

            return webResponse;
        }

        public WebResponse CheckSerial(string orderNum, string serialId)
        {
            WebResponse webResponse = new WebResponse();

            string parsedSerialId = serialId;
            int parsingLength = System.Convert.ToInt32(ConfigurationManager.AppSettings[Constants.PARSINGLENGTH]);

            //remvoe () if any
            parsedSerialId = parsedSerialId.Replace("(", string.Empty);
            parsedSerialId = parsedSerialId.Replace(")", string.Empty);
            //select last parsingLength digits from the string
            parsedSerialId = parsedSerialId.Substring(parsedSerialId.Length - parsingLength, parsingLength);

            Logger.LogInfo(string.Format("OrderNum: {0} - SerialId: {1} - ParsedSerialId: {2}", orderNum, serialId, parsedSerialId));

            var provider = Providers.ProviderFactory.GetWarehouseProvider();
            var response = provider.CheckSerial(orderNum, parsedSerialId);

            webResponse = response.Value;

            return webResponse;
        }

        public WebResponse CheckStageLP(string licensePlate)
        {
            WebResponse webResponse = new WebResponse();

            Logger.LogInfo(string.Format("LicensePlate: {0}", licensePlate));

            var provider = Providers.ProviderFactory.GetWarehouseProvider();
            var response = provider.CheckStageLP(licensePlate);

            webResponse = response.Value;

            return webResponse;
        }

        public WebResponse PickSerial(PickSerialRequest request)
        {
            WebResponse webResponse = new WebResponse();

            string parsedSerialId = request.SerialId;
            int parsingLength = System.Convert.ToInt32(ConfigurationManager.AppSettings[Constants.PARSINGLENGTH]);

            //remvoe () if any
            parsedSerialId = parsedSerialId.Replace("(", string.Empty);
            parsedSerialId = parsedSerialId.Replace(")", string.Empty);
            //select last parsingLength digits from the string
            parsedSerialId = parsedSerialId.Substring(parsedSerialId.Length - parsingLength, parsingLength);

            Logger.LogInfo(string.Format("OrderNum: {0} - SerialId: {1} - LicensePlate: {2} - ParsedSerialId: {3}", request.OrderNum, request.SerialId, request.LicensePlateId, parsedSerialId));

            request.SerialId = parsedSerialId;

            var provider = Providers.ProviderFactory.GetWarehouseProvider();
            var response = provider.PickSerial(request);

            webResponse = response.Value;


            return webResponse;
        }

        public WebResponse CheckAndPickSerial(PickSerialRequest request)
        {
            WebResponse webResponse = new WebResponse();

            string parsedSerialId = request.SerialId;
            int parsingLength = System.Convert.ToInt32(ConfigurationManager.AppSettings[Constants.PARSINGLENGTH]);

            //remvoe () if any
            parsedSerialId = parsedSerialId.Replace("(", string.Empty);
            parsedSerialId = parsedSerialId.Replace(")", string.Empty);
            //select last parsingLength digits from the string
            parsedSerialId = parsedSerialId.Substring(parsedSerialId.Length - parsingLength, parsingLength);

            Logger.LogInfo(string.Format("OrderNum: {0} - SerialId: {1} - LicensePlate: {2} - ParsedSerialId: {3}", request.OrderNum, request.SerialId, request.LicensePlateId, parsedSerialId));

            var provider = Providers.ProviderFactory.GetWarehouseProvider();
            var response = provider.CheckSerial(request.OrderNum, parsedSerialId);

            if(response.Success)
            {
                request.SerialId = parsedSerialId;
                response = provider.PickSerial(request);
            }

            webResponse = response.Value;

            return webResponse;
        }

        public WebResponse CheckAndPickSerial_V2(string orderNum, string licensePlate, string serialId)
        {
            WebResponse webResponse = new WebResponse();

            string parsedSerialId = serialId;
            int parsingLength = System.Convert.ToInt32(ConfigurationManager.AppSettings[Constants.PARSINGLENGTH]);

            //remvoe () if any
            parsedSerialId = parsedSerialId.Replace("(", string.Empty);
            parsedSerialId = parsedSerialId.Replace(")", string.Empty);
            //select last parsingLength digits from the string
            parsedSerialId = parsedSerialId.Substring(parsedSerialId.Length - parsingLength, parsingLength);

            Logger.LogInfo(string.Format("OrderNum: {0} - SerialId: {1} - LicensePlate: {2} - ParsedSerialId: {3}", orderNum, serialId, licensePlate, parsedSerialId));

            var provider = Providers.ProviderFactory.GetWarehouseProvider();
            var response = provider.CheckSerial(orderNum, parsedSerialId);

            if (response.Success)
            {
                PickSerialRequest request = new PickSerialRequest() { LicensePlateId = licensePlate, OrderNum = orderNum, SerialId = parsedSerialId };
                response = provider.PickSerial(request);
            }

            webResponse = response.Value;

            return webResponse;
        }
    }
}