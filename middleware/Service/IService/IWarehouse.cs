﻿using AgriBeef.WarehouseMW.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Threading.Tasks;

namespace AgriBeef.WarehouseMW
{
    public partial interface IService
    {
        [OperationContract,
        WebInvoke(UriTemplate = "/warehouse/orderNum/{orderNum}",
                 Method = "GET")]
        WebResponse CheckOrderNum(string orderNum);

        [OperationContract,
        WebInvoke(UriTemplate = "/warehouse/serial/{orderNum}/{serialId}", 
                 Method = "GET")]
        WebResponse CheckSerial(string orderNum, string serialId);

        [OperationContract,
        WebInvoke(UriTemplate = "/warehouse/licenseplate/{licensePlate}",
                 Method = "POST")]
        WebResponse CheckStageLP(string licensePlate);

        [OperationContract,
        WebInvoke(UriTemplate = "/warehouse/pickserial",
                 Method = "POST")]
        WebResponse PickSerial(PickSerialRequest request);

        [OperationContract,
        WebInvoke(UriTemplate = "/warehouse/checkandpickserial",
                 Method = "POST")]
        WebResponse CheckAndPickSerial(PickSerialRequest request);

        [OperationContract,
        WebInvoke(UriTemplate = "/warehouse/checkandpickserial/v2/{orderNum}/{licensePlate}/{serialId}",
                 Method = "POST")]
        WebResponse CheckAndPickSerial_V2(string orderNum, string licensePlate, string serialId);
    }
}
