﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace AgriBeef.WarehouseMW.Resources {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    internal class Messages {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal Messages() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("AgriBeef.WarehouseMW.Resources.Messages", typeof(Messages).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Some Error Occured while processing request..
        /// </summary>
        internal static string Err_01 {
            get {
                return ResourceManager.GetString("Err_01", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Some Error Occured while communiction with AX..
        /// </summary>
        internal static string Err_02 {
            get {
                return ResourceManager.GetString("Err_02", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Please supply proper value in __..
        /// </summary>
        internal static string Err_03 {
            get {
                return ResourceManager.GetString("Err_03", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Supplied Entity type is not supported..
        /// </summary>
        internal static string Err_04 {
            get {
                return ResourceManager.GetString("Err_04", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Some Error Occured while save contact information..
        /// </summary>
        internal static string Err_05 {
            get {
                return ResourceManager.GetString("Err_05", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Please supply value for &apos;{0}&apos;..
        /// </summary>
        internal static string Err_06 {
            get {
                return ResourceManager.GetString("Err_06", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to &apos;{0}&apos; length should not exceeed {1} characters..
        /// </summary>
        internal static string Err_07 {
            get {
                return ResourceManager.GetString("Err_07", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Some Error Occured while Executing database query..
        /// </summary>
        internal static string Err_08 {
            get {
                return ResourceManager.GetString("Err_08", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Some Error Occured while subscribing user to Mail Chimp..
        /// </summary>
        internal static string Err_09 {
            get {
                return ResourceManager.GetString("Err_09", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Some Error Occured while calling Rest API..
        /// </summary>
        internal static string Err_10 {
            get {
                return ResourceManager.GetString("Err_10", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Mail Chimp Error Content : {0}. .
        /// </summary>
        internal static string Err_11 {
            get {
                return ResourceManager.GetString("Err_11", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to No Data exist against id: {0}..
        /// </summary>
        internal static string Err_12 {
            get {
                return ResourceManager.GetString("Err_12", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to MS Dynamics AX is unreachable..
        /// </summary>
        internal static string Err_13 {
            get {
                return ResourceManager.GetString("Err_13", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Unauthorized Access....
        /// </summary>
        internal static string Err_14 {
            get {
                return ResourceManager.GetString("Err_14", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Invalid API Key Supplied..
        /// </summary>
        internal static string Err_15 {
            get {
                return ResourceManager.GetString("Err_15", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to The login credentials you provided are not valid.  Please try again. Contact Folio3 for technical assistance if you continue to have difficulty..
        /// </summary>
        internal static string Err_16 {
            get {
                return ResourceManager.GetString("Err_16", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to The system is unavailable. Please contact Folio3 for technical assistance..
        /// </summary>
        internal static string Err_17 {
            get {
                return ResourceManager.GetString("Err_17", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to The Customer, Company, and/or Environment is not configured. Please contact Folio3 Express for technical assistance..
        /// </summary>
        internal static string Err_18 {
            get {
                return ResourceManager.GetString("Err_18", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to May be File not exist for supplied id..
        /// </summary>
        internal static string Err_19 {
            get {
                return ResourceManager.GetString("Err_19", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Customer is unauthorized  to access application. Please contact support for technical assistance..
        /// </summary>
        internal static string Err_20 {
            get {
                return ResourceManager.GetString("Err_20", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Application license has been expired. Please contact support for further assistance..
        /// </summary>
        internal static string Err_21 {
            get {
                return ResourceManager.GetString("Err_21", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to User is unauthorized  to access application. Please contact support for technical assistance..
        /// </summary>
        internal static string Err_22 {
            get {
                return ResourceManager.GetString("Err_22", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Application license will be expired in {0} day(s). Please contact support for further assistance..
        /// </summary>
        internal static string Err_23 {
            get {
                return ResourceManager.GetString("Err_23", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to License file invalid. Please contact support for further assistance..
        /// </summary>
        internal static string Err_24 {
            get {
                return ResourceManager.GetString("Err_24", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to User CSV license file is invalid. Please contact support for further assistance..
        /// </summary>
        internal static string Err_25 {
            get {
                return ResourceManager.GetString("Err_25", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to User CSV license file is in use by another program. Please close and try again..
        /// </summary>
        internal static string Err_26 {
            get {
                return ResourceManager.GetString("Err_26", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to App outdated, current version of app is not supported..
        /// </summary>
        internal static string Err_27 {
            get {
                return ResourceManager.GetString("Err_27", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Maintenance is in progress..
        /// </summary>
        internal static string Err_28 {
            get {
                return ResourceManager.GetString("Err_28", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Application session has expired. Please login again. Thanks..
        /// </summary>
        internal static string Err_29 {
            get {
                return ResourceManager.GetString("Err_29", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to User having Email &apos;{0}&apos; is successfully subscribed to Mail Chimp at &apos;{1}&apos;..
        /// </summary>
        internal static string Info_01 {
            get {
                return ResourceManager.GetString("Info_01", resourceCulture);
            }
        }
    }
}
