﻿using AgriBeef.WarehouseMW.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace AgriBeef.WarehouseMW
{
    public partial class Service : IService
    {
        /// <summary>
        /// Echo method
        /// </summary>
        /// <param name="echoString">echo string.</param>
        /// <returns></returns>
        public String Echo(String echoString)
        {
            return string.Format("Warehouse MW >> {0}",  echoString);
        }
    }
}
