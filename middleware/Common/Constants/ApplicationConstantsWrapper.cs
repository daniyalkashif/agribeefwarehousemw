﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace AgriBeef.WarehouseMW.Common.Constants
{
    // <summary>
    /// Application constants wrapper
    /// </summary>
    public class ApplicationConstantsWrapper
    {
        public static Uri SerivceAddress
        {
            get
            {
                return new Uri(ConfigurationManager.AppSettings[Constants.URI]);
            }
        }
        public static string UPNIdentity
        {
            get
            {
                return ConfigurationManager.AppSettings[Constants.UPNIDENTITY].ToString();
            }
        }
        public static string DataArea
        {
            get
            {
                return ConfigurationManager.AppSettings[Constants.DATAAREA].ToString();
            }
        }
    }
}