﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace AgriBeef.WarehouseMW.Common.Constants
{
    // <summary>
    /// constant strings
    /// </summary>
    public static class Constants
    {
        public const string HEADER_KEY_APPMODE = "AppMode";
        public const string HEADER_KEY_USERAGENT = "User-Agent";

        #region Configuration
        public const string DATAAREA = "dataArea";
        public const string URI = "uri";
        public const string UPNIDENTITY = "uPnIdentity";
        public const string PARSINGLENGTH = "ParsingLength";
        #endregion
    }

    
}