﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AgriBeef.WarehouseMW.Common
{
    public enum APIMode : int
    {
        /// <summary>
        /// Live Mode
        /// </summary>
        LIVE,
        /// <summary>
        /// Trial Mode
        /// </summary>
        TRIAL,
        /// <summary>
        /// Trial Mode
        /// </summary>
        D365_FinanceAndOperations
    }

    #region Notification Enums
    /// <summary>
    /// Supported devices enum for push notifications
    /// </summary>
    public enum Device { Android, iOS, Web };

    /// <summary>
    /// Supported providers for push notifications
    /// </summary>
    public enum NotificationProvider { Azure, PushSharp };

    /// <summary>
    /// Alert type for work item notification
    /// </summary>
    public enum AlertType { Assigned, Due };
    #endregion

    #region HTTP Enums
    public enum HTTP_Methods { 
        GET,
        POST,
        PUT,
        DELETE,
        PATCH
    }
    #endregion

    public enum ValidationType
    {
        Success,
        Information,
        Warning,
        Error,
        Critical,
        Exception
    }

}