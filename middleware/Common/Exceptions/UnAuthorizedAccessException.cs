﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AgriBeef.WarehouseMW.Common.Exceptions
{
    public class UnAuthorizedAccessException : ApplicationException
    {
        public int ErrorCode { get; set; }
        public string ErrorMessage { get; set; }

        public UnAuthorizedAccessException(int errorCode, string errorMessage)
        {
            this.ErrorCode = errorCode;
            this.ErrorMessage = errorMessage;
        }
    }
}