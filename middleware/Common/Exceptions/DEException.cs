﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace AgriBeef.WarehouseMW.Common.Exceptions
{
    [Serializable]
    public class DEException : Exception
    {
        #region Fields

        private int errorCode;
        private List<string> errorMessages;

        #endregion

        #region Properties

        public int ErrorCode
        {
            get
            {
                // Summary:
                //     Equivalent to HTTP status 412. System.Net.HttpStatusCode.PreconditionFailed
                //     indicates that a condition set for this request failed, and the request cannot
                //     be carried out. Conditions are set with conditional request headers like
                //     If-Match, If-None-Match, or If-Unmodified-Since.

                if (this.errorCode == 0)
                    return 412; // 
                return this.errorCode;
            }
        }

        public List<string> ErrorMessages
        {
            get
            {
                if (errorMessages == null) errorMessages = new List<string>();
                return errorMessages;
            }
        }

        #endregion

        #region Constructors

        public DEException() { }

        public DEException(string message) : base(message) { }

        public DEException(int errorCode) { this.errorCode = errorCode; }

        public DEException(string message, int errorCode) : base(message) { this.errorCode = errorCode; }

        public DEException(string message, Exception innerExc) : base(message, innerExc) { }

        public DEException(string message, int errorCode, Exception innerExc) : base(message, innerExc) { this.errorCode = errorCode; }

        protected DEException(SerializationInfo info, StreamingContext context) : base(info, context) { }

        #endregion

        #region Methods

        public void AddErrorMessage(string errMessage)
        {
            if (errorMessages == null) errorMessages = new List<string>();
            errorMessages.Add(errMessage);
        }
        public void AddErrorMessage(List<string> errMessages)
        {
            if (errorMessages == null) errorMessages = new List<string>();
            errorMessages.AddRange(errMessages);
        }

        #endregion
    }
}