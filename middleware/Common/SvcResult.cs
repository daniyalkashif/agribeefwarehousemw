﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AgriBeef.WarehouseMW.Common
{
    public class SvcResult
    {
        #region Constructors
        public SvcResult()
        { }

        public SvcResult(bool successful) : this(successful, string.Empty)
        { }

        public SvcResult(bool successful, string msg) : this(successful, msg, null)
        { }

        public SvcResult(bool successful, string msg, Exception exception)
        {
            Success = successful;
            Message = msg;
            Exception = exception;
        }
        public SvcResult(bool successful, string msg, Exception exception, ValidationType type)
        {
            Success = successful;
            Message = msg;
            Exception = exception;
            Type = type;
        }
        #endregion

        private bool success = true;
        public bool Success
        {
            get { return success; }
            set { success = value; }
        }
        private Exception ex;
        public Exception Exception
        {
            get { return ex; }
            set { ex = value; }
        }
        private string message;
        public string Message
        {
            get { return message; }
            set { message = value; }
        }

        private ValidationType type = ValidationType.Success;
        public ValidationType Type
        {
            get { return type; }
            set { type = value; }
        }
    }

    public class SvcResult<T> : SvcResult
    {
        #region Constructors
        /// <summary>
        /// Initializes a new instance of the <see cref="SvcResult{T}"/> class.
        /// Defaults <see cref="SvcResult.Successful"/> to <c>true</c>
        /// </summary>
        public SvcResult() : base(true, string.Empty, null)
        { }

        /// <summary>
        /// Initializes a new instance of the <see cref="SvcResult{T}"/> class.
        /// </summary>
        /// <param name="successful">if set to <c>true</c> [successful].</param>
        /// <param name="message">The message.</param>
        /// <param name="ex">The ex.</param>
        public SvcResult(bool successful, string message, Exception ex) : base(successful, message, ex)
        { }

        /// <summary>
        /// Initializes a new instance of the <see cref="SvcResult{T}"/> class.
        /// Defaults <see cref="SvcResult.Successful"/> to <c>true</c>
        /// </summary>
        /// <param name="value">The value.</param>
        public SvcResult(T value) : this(value, true, string.Empty, null)
        { }

        /// <summary>
        /// Initializes a new instance of the <see cref="SvcResult{T}"/> class.
        /// Defaults <see cref="SvcResult.Successful"/> to <c>true</c>
        /// </summary>
        /// <param name="value">The value.</param>
        /// <param name="message">The message.</param>
        public SvcResult(T value, string message) : this(value, true, message, null)
        { }

        /// <summary>
        /// Initializes a new instance of the <see cref="SvcResult{T}"/> class.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <param name="successful">if set to <c>true</c> [successful].</param>
        /// <param name="message">The message.</param>
        public SvcResult(T value, bool successful, string message) :
            this(value, successful, message, null)
        { }

        /// <summary>
        /// Initializes a new instance of the <see cref="SvcResult{T}"/> class.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <param name="successful">if set to <c>true</c> [successful].</param>
        /// <param name="message">The message.</param>
        /// <param name="ex">The ex.</param>
        public SvcResult(T value, bool successful, string message, Exception ex) :
            base(successful, message, ex)
        {
            Value = value;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="SvcResult{T}"/> class.
        /// </summary>
        /// <param name="value"></param>
        /// <param name="successful"></param>
        /// <param name="message"></param>
        /// <param name="ex"></param>
        /// <param name="type"></param>
        public SvcResult(T value, bool successful, string message, Exception ex, ValidationType type) :
            base(successful, message, ex, type)
        {
            Value = value;
        }


        #endregion

        /// <summary>
        /// Gets or sets the value.
        /// </summary>
        /// <value>
        /// The value.
        /// </value>
        public T Value { get; set; }
    }


}