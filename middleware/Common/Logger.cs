﻿using AgriBeef.WarehouseMW.Types;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;

namespace AgriBeef.WarehouseMW.Common
{
    // <summary>
    /// A Logger responsible for logging (use log4Net)
    /// </summary>
    public class Logger
    {
        const string PACKET_LOGGER = "PacketLogger";
        const string ERROR_LOGGER = "ErrorLogger";

        static log4net.ILog packetLogger;
        static log4net.ILog errorLogger;

        static void InitializeLoggers()
        {
        }

        static Logger()
        {
            packetLogger = log4net.LogManager.GetLogger(PACKET_LOGGER);
            errorLogger = log4net.LogManager.GetLogger(ERROR_LOGGER);
        }

        public static void LogPacket(RequestPacket req_packet)
        {
            // prepare message for logging
            string message = string.Format("Method: {0} | ReqProcessedIn: {1}s | URL: {2} | Agent: {3}",
                                                req_packet.Method.PadRight(4),
                                                req_packet.ProcessedIn.ToString("0.000").PadLeft(6, '0'),
                                                req_packet.RequestUrl,
                                                req_packet.UserAgent);
            
            if (!string.IsNullOrEmpty(req_packet.User))
            {
                message = string.Format("{0} | User: {1}", message, req_packet.User);
            }
            if (!string.IsNullOrEmpty(req_packet.QueryParams))
            {
                message = string.Format("{0} | QueryParams: {1}", message, req_packet.QueryParams);
            }
            if (!string.IsNullOrEmpty(req_packet.RequestBody))
            {
                message = string.Format("{0} | RequestBody: {1}", message, req_packet.RequestBody);
            }
            if (!string.IsNullOrEmpty(req_packet.ReplyBody))
            {
                message = string.Format("{0} | ReplyBody: {1}", message, req_packet.ReplyBody);
            }
            packetLogger.Info(message);
        }

        public static void LogException(string message)
        {
            errorLogger.Error(message);
        }

        public static void LogException(string message, Exception ex)
        {
            errorLogger.Error(message, ex);
        }

        public static void LogInfo(string message)
        {
            errorLogger.Info(message);
        }
    }
}