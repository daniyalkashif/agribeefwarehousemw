﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Net;
using System.Runtime.Serialization.Json;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Description;
using System.ServiceModel.Dispatcher;
using System.ServiceModel.Web;

using AgriBeef.WarehouseMW.Common.Exceptions;

namespace AgriBeef.WarehouseMW.Common.ServiceBehaviorExtensions
{
    /// <summary>
    /// SFS Service Error Handler
    /// </summary>
    public class ServiceErrorHandler : IErrorHandler, IServiceBehavior
    {
        #region IErrorHandler Members

        /// <summary>
        /// Enables error-related processing and returns a value that indicates whether the dispatcher aborts the session and the instance context in certain cases.
        /// </summary>
        /// <param name="error">The exception thrown during processing.</param>
        /// <returns>
        /// true if  should not abort the session (if there is one) and instance context if the instance context is not <see cref="F:System.ServiceModel.InstanceContextMode.Single" />; otherwise, false. The default is false.
        /// </returns>
        public bool HandleError(Exception error)
        {
            // log error
            Logger.LogException(error.Message, error);

            return true;
        }

        /// <summary>
        /// Enables the creation of a custom <see cref="T:System.ServiceModel.FaultException`1" /> that is returned from an exception in the course of a service method.
        /// </summary>
        /// <param name="error">The <see cref="T:System.Exception" /> object thrown in the course of the service operation.</param>
        /// <param name="version">The SOAP version of the message.</param>
        /// <param name="fault">The <see cref="T:System.ServiceModel.Channels.Message" /> object that is returned to the client, or service, in the duplex case.</param>
        public void ProvideFault(Exception error, MessageVersion version, ref Message fault)
        {
            string message = Resources.Messages.Err_01.ToString();
            HttpStatusCode errorCode = HttpStatusCode.InternalServerError;
            List<string> errMessages = new List<string>();

            //general error
            if (error.GetType() == typeof(System.Exception))
            {
                errMessages.Add(Resources.Messages.Err_01.ToString());
            }

            //SFS error
            else if (error.GetType() == typeof(DEException))
            {
                DEException e = error as DEException;
                errorCode = (HttpStatusCode)Enum.ToObject(typeof(HttpStatusCode), e.ErrorCode);
                errMessages.AddRange(e.ErrorMessages);
            }

            // unauthorized access error
            else if (error.GetType() == typeof(UnAuthorizedAccessException))
            {
                UnAuthorizedAccessException e = error as UnAuthorizedAccessException;
                errorCode = (HttpStatusCode)Enum.ToObject(typeof(HttpStatusCode), e.ErrorCode); //HttpStatusCode.Forbidden;
                errMessages.Add(e.ErrorMessage);
            }

            //any other type
            else
            {
                errMessages.Add(personalizeErrorMessage(error));
            }

            // create fault exception object
            var serviceError = new ServiceError { ErrorCode = (int)errorCode, ErrorMessage = errMessages.ToArray() };
            fault = Message.CreateMessage(version, null, serviceError, new DataContractJsonSerializer(typeof(ServiceError)));

            // return fault in json format
            var wbf = new WebBodyFormatMessageProperty(WebContentFormat.Json);
            fault.Properties.Add(WebBodyFormatMessageProperty.Name, wbf);

            var response = WebOperationContext.Current.OutgoingResponse;
            response.ContentType = "application/json";
            response.StatusCode = errorCode;
        }

        /// <summary>
        /// Personalizes the error message.
        /// </summary>
        /// <param name="msg">The MSG.</param>
        /// <returns></returns>
        private string personalizeErrorMessage(Exception error)
        {
            // TODO: send user friendly messages
            return error.Message;
        }

        #endregion

        #region IServiceBehavior Members

        /// <summary>
        /// Provides the ability to change run-time property values or insert custom extension objects such as error handlers, message or parameter interceptors, security extensions, and other custom extension objects.
        /// </summary>
        /// <param name="serviceDescription">The service description.</param>
        /// <param name="serviceHostBase">The host that is currently being built.</param>
        public void ApplyDispatchBehavior(ServiceDescription serviceDescription, ServiceHostBase serviceHostBase)
        {
            IErrorHandler errorHandler = new ServiceErrorHandler();

            // Apply ServiceErrorHandler to all service methods
            foreach (ChannelDispatcherBase channelDispatcherBase in serviceHostBase.ChannelDispatchers)
            {
                ChannelDispatcher channelDispatcher = channelDispatcherBase as ChannelDispatcher;
                channelDispatcher.ErrorHandlers.Add(errorHandler);
            }
        }

        /// <summary>
        /// Provides the ability to pass custom data to binding elements to support the contract implementation.
        /// </summary>
        /// <param name="serviceDescription">The service description of the service.</param>
        /// <param name="serviceHostBase">The host of the service.</param>
        /// <param name="endpoints">The service endpoints.</param>
        /// <param name="bindingParameters">Custom objects to which binding elements have access.</param>
        public void AddBindingParameters(ServiceDescription serviceDescription, ServiceHostBase serviceHostBase, Collection<ServiceEndpoint> endpoints, BindingParameterCollection bindingParameters)
        {

        }

        /// <summary>
        /// Provides the ability to inspect the service host and the service description to confirm that the service can run successfully.
        /// </summary>
        /// <param name="serviceDescription">The service description.</param>
        /// <param name="serviceHostBase">The service host that is currently being constructed.</param>
        public void Validate(ServiceDescription serviceDescription, ServiceHostBase serviceHostBase)
        {

        }

        #endregion
    }
}