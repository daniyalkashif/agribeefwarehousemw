﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.Configuration;
using System.Web;

namespace AgriBeef.WarehouseMW.Common.ServiceBehaviorExtensions
{
    /// <summary>
    /// Service Error Handler Behavior
    /// </summary>
    public class ServiceErrorHandlerBehavior : BehaviorExtensionElement
    {
        /// <summary>
        /// Gets the type of behavior.
        /// </summary>
        /// <returns>A <see cref="T:System.Type" />.</returns>
        public override Type BehaviorType
        {
            get
            {
                return typeof(ServiceErrorHandler);
            }
        }

        /// <summary>
        /// Creates a behavior extension based on the current configuration settings.
        /// </summary>
        /// <returns>
        /// The behavior extension.
        /// </returns>
        protected override object CreateBehavior()
        {
            return new ServiceErrorHandler();
        }
    }
}