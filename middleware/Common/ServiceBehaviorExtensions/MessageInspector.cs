﻿using System;
using System.IO;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Description;
using System.ServiceModel.Dispatcher;
using System.Xml;
using AgriBeef.WarehouseMW.Common.Exceptions;
using AgriBeef.WarehouseMW.Types;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace AgriBeef.WarehouseMW.Common.ServiceBehaviorExtensions
{
    /// <summary>
    /// Message Inspector
    /// </summary>
    public class MessageInspector : IDispatchMessageInspector, IEndpointBehavior
    {
        #region IDispatchMessageInspector Members
        
        /// <summary>
        /// Called after an inbound message has been received but before the message is dispatched to the intended operation.
        /// </summary>
        /// <param name="request">The request message.</param>
        /// <param name="channel">The incoming channel.</param>
        /// <param name="instanceContext">The current service instance.</param>
        /// <returns>
        /// The object used to correlate state. This object is passed back in the <see cref="M:System.ServiceModel.Dispatcher.IDispatchMessageInspector.BeforeSendReply(System.ServiceModel.Channels.Message@,System.Object)" /> method.
        /// </returns>
        public object AfterReceiveRequest(ref Message request, IClientChannel channel, InstanceContext instanceContext)
        {
            DateTime reqArrivedAt = DateTime.Now;
            
            // inspect and fetch the request items
            var httpRequestMessageProp = (HttpRequestMessageProperty)request.Properties[HttpRequestMessageProperty.Name];
           
            string messageBody = string.Empty;
            // If request was not of auth, parse request body.
            if (!this.IsCallExcluded(request.Properties.Via.AbsoluteUri, httpRequestMessageProp.Method))
            {
                var buffer = request.CreateBufferedCopy(int.MaxValue);
                request = buffer.CreateMessage();
                messageBody  = this.GetBody(buffer);
            }

            RequestPacket req_packet = new RequestPacket
            {
                UserAgent = httpRequestMessageProp.Headers[Constants.Constants.HEADER_KEY_USERAGENT],
                RequestUrl = request.Properties.Via.AbsoluteUri,
                Method = httpRequestMessageProp.Method,
                QueryParams = httpRequestMessageProp.QueryString,
                RequestBody = messageBody,
                ArrivedAt = reqArrivedAt
            };

            // return object which will be passed to BeforeSendReply to correlationState param
            return req_packet;
        }

        /// <summary>
        /// Called after the operation has returned but before the reply message is sent.
        /// </summary>
        /// <param name="reply">The reply message. This value is null if the operation is one way.</param>
        /// <param name="correlationState">The correlation object returned from the <see cref="M:System.ServiceModel.Dispatcher.IDispatchMessageInspector.AfterReceiveRequest(System.ServiceModel.Channels.Message@,System.ServiceModel.IClientChannel,System.ServiceModel.InstanceContext)" /> method.</param>
        public void BeforeSendReply(ref  Message reply, object correlationState)
        {
            if (correlationState != null)
            {
                // extract traffic info
                RequestPacket req_packet = (RequestPacket)correlationState;

                // set the response time
                req_packet.DispatchedAt = DateTime.Now;

                // If request was not of auth, parse reply body.
                if (!this.IsCallExcluded(req_packet.RequestUrl, req_packet.Method))
                {
                    // add reply body into the req_packet
                    var buffer = reply.CreateBufferedCopy(int.MaxValue);
                    reply = buffer.CreateMessage();
                    req_packet.ReplyBody = this.GetBody(buffer);
                }

                // log packet
                Logger.LogPacket(req_packet);
            }
        }

        #endregion

        #region IEndpointBehavior Members

        /// <summary>
        /// AddBindingParameters
        /// </summary>
        /// <param name="endpoint"></param>
        /// <param name="bindingParameters"></param>
        public void AddBindingParameters(ServiceEndpoint endpoint, BindingParameterCollection bindingParameters)
        {
        }

        /// <summary>
        /// ApplyClientBehavior
        /// </summary>
        /// <param name="endpoint"></param>
        /// <param name="clientRuntime"></param>
        public void ApplyClientBehavior(ServiceEndpoint endpoint, ClientRuntime clientRuntime)
        {
        }

        /// <summary>
        /// ApplyDispatchBehavior
        /// </summary>
        /// <param name="endpoint"></param>
        /// <param name="endpointDispatcher"></param>
        public void ApplyDispatchBehavior(ServiceEndpoint endpoint, EndpointDispatcher endpointDispatcher)
        {
            endpointDispatcher.DispatchRuntime.MessageInspectors.Add(this);
        }

        /// <summary>
        /// Validate
        /// </summary>
        /// <param name="endpoint"></param>
        public void Validate(ServiceEndpoint endpoint)
        {
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Collect Authorization Token from request header
        /// </summary>
        /// <param name="httpRequestMessageProp"></param>
        /// <returns></returns>
        private static string CollectAuthorizationToken(HttpRequestMessageProperty httpRequestMessageProp)
        {
            /*
             * Try collect Token from Header
             * then try collect token from querystring
             */
            string auth_token = httpRequestMessageProp.Headers["Authorization"];
            if (string.IsNullOrEmpty(auth_token))
            {
                string[] queryStrings = httpRequestMessageProp.QueryString.Split('&');
                if (queryStrings.Any())
                {
                    // expect 'Token' at 0 index
                    string tokenKey = queryStrings[0];
                    int index = tokenKey.IndexOf('=', 0);

                    auth_token = tokenKey.Substring(index + 1, tokenKey.Length - index - 1);
                }
            }

            return auth_token;
        }

        /// <summary>
        /// Collect API Mode; default Mode is Live if not supplied.
        /// </summary>
        /// <example name="SFSException">Incase of Invalid Token Supplied.</example>
        /// <param name="httpRequestMessageProp"></param>
        /// <returns></returns>
        private static Common.APIMode CollectAPIModeToken(HttpRequestMessageProperty httpRequestMessageProp)
        {
            /*
             * Try collect Token from Header
             * defualt should be live incase data not supplied
             */
            Common.APIMode apiMode;
            string requestedAPIMode = httpRequestMessageProp.Headers["API_Mode"];
            if (string.IsNullOrWhiteSpace(requestedAPIMode))
            {
                string[] queryStrings = httpRequestMessageProp.QueryString.Split('&');
                if (queryStrings.Any())
                {
                    // expect 'Token' at 0 index
                    string[] token = queryStrings[0].Split('=');
                    if (token[0].ToLower().Equals("api_mode"))
                    {
                        requestedAPIMode = token[1];
                    }
                }
            }

            if (string.IsNullOrWhiteSpace(requestedAPIMode))
                return Common.APIMode.LIVE; // default to Live
            else if (Enum.TryParse<Common.APIMode>(requestedAPIMode.ToUpper(), out apiMode)) // validate supplied data
            {
                return apiMode;
            }
            else
            {
                DEException ex = new DEException();
                ex.AddErrorMessage("Inalid API_Mode supplied.");
                throw ex;
            }
        }
        
        /// <summary>
        /// Gets the body from a WCF Message.
        /// </summary>
        /// <param name="buffer">In memory buffer that stores the copy of the current wcf message</param>
        /// <returns></returns>
        private string GetBody(MessageBuffer buffer)
        {
            var originalMessage = buffer.CreateMessage();
            string messageContent;
            string jsonText = string.Empty;

            try
            {
                using (StringWriter stringWriter = new StringWriter())
                {
                    using (XmlTextWriter xmlTextWriter = new XmlTextWriter(stringWriter))
                    {
                        originalMessage.WriteMessage(xmlTextWriter);
                        xmlTextWriter.Flush();
                        xmlTextWriter.Close();
                    }
                    messageContent = stringWriter.ToString();
                }

                if (!string.IsNullOrEmpty(messageContent))
                {
                    // Convert an XML node contained in string xml into a JSON string   
                    XmlDocument doc = new XmlDocument();
                    doc.LoadXml(messageContent);
                    jsonText = JsonConvert.SerializeXmlNode(doc);
                    jsonText = jsonText.Replace("@", "").Replace("#", "");
                }
            }
            catch(Exception ex)
            {
                Logger.LogException(ex.ToString());
            }
            return jsonText;
        }

        private bool IsCallExcluded(string apiEndpoint, string method)
        {
            bool isExcluded = false;

            if (method == HTTP_Methods.GET.ToString())
            {
                isExcluded = true;
            }
            else
            {
                string[] notBodyRequiredEndpoints = new string[] {
                    "/json/auth",
                    "/json/echo",
                    "/json/metadata",
                    "/json/approval_metadata"
                };

                foreach (var endpoint in notBodyRequiredEndpoints)
                {
                    if (apiEndpoint.ToLower().Contains(endpoint.ToLower()))
                    {
                        isExcluded = true;
                        break;
                    }
                }
            }
            return isExcluded;
        }

        #endregion
    }
}