﻿using AgriBeef.WarehouseMW.Common.Constants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AgriBeef.WarehouseMW.Providers
{
    public class ProviderFactory
    {
        public static IWarehouseProvider GetWarehouseProvider()
        {
            return new AX2012.WarehouseProvider(ApplicationConstantsWrapper.SerivceAddress, ApplicationConstantsWrapper.UPNIdentity, ApplicationConstantsWrapper.DataArea);
        }
    }
}