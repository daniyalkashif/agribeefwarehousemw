﻿using AgriBeef.WarehouseMW.Common;
using AgriBeef.WarehouseMW.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AgriBeef.WarehouseMW.Providers
{
    public interface IWarehouseProvider
    {
        SvcResult<WebResponse> CheckOrderNum(string orderNum);
        SvcResult<WebResponse> CheckSerial(string orderNum, string serialId);
        SvcResult<WebResponse> CheckStageLP(string licensePlate);
        SvcResult<WebResponse> PickSerial(PickSerialRequest request);
    }
}
