﻿using System;
using System.ServiceModel;

namespace AgriBeef.WarehouseMW.Providers
{
    internal static class ServiceClientFactory<T> where T : IClientChannel
    {
        /// <summary>
        /// Gets the client channel.
        /// </summary>
        /// <param name="endpointAddress">The endpoint address.</param>
        /// <param name="upnIdentity">The upn identity.</param>
        /// <returns></returns>
        /// <exception cref="System.ArgumentException">
        /// An endpoint address must be specified.
        /// or
        /// A UPN identity must be specified.
        /// </exception>
        public static T GetClient(Uri endpointAddress, string upnIdentity)
        {
            if (endpointAddress == null)
                throw new ArgumentException("An endpoint address must be specified.");

            if (upnIdentity == null)
                throw new ArgumentException("A UPN identity must be specified.");

            var binding = new NetTcpBinding { SendTimeout = new TimeSpan(0, 10, 0) };
            var endpoint = new EndpointAddress(endpointAddress, new UpnEndpointIdentity(upnIdentity));
            var channelFactory = new ChannelFactory<T>(binding, endpoint);
            
            var client = channelFactory.CreateChannel();

            

            return client;
        }
    }
}