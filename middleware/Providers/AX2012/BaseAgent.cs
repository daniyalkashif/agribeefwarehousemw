﻿using AgriBeef.WarehouseMW.WHSMobileValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AgriBeef.WarehouseMW.Providers.AX2012
{
    public abstract class BaseAgent
    {
        /// <summary>
        /// Initializes a new instance of a <see cref="BaseAgent"/> class.
        /// </summary>
        /// <param name="serviceAddress">The service address.</param>
        /// <param name="upnIdentity">The upn identity.</param>
        /// <param name="dataArea">The DAX data area.</param>
        /// <exception cref="System.ArgumentException">
        /// A service address must be specified.
        /// or
        /// The service address is not a valid Uri.
        /// or
        /// A UPN identity must be specified.
        /// </exception>
        protected BaseAgent(string serviceAddress, string upnIdentity, string dataArea)
        {
            Uri serviceUri ;

            if (string.IsNullOrEmpty(serviceAddress))
                throw new ArgumentException("A service address must be specified.");

            if (Uri.TryCreate(serviceAddress, UriKind.Absolute, out serviceUri))
                ServiceAddress = serviceUri;
            else
                throw new ArgumentException($"The service address {serviceAddress} is not a valid Uri.");

            if (string.IsNullOrEmpty(upnIdentity))
                throw new ArgumentException("A UPN identity must be specified.");

            UpnIdentity = upnIdentity;

            CallContext = new CallContext
            {
                Company = dataArea,
                Language = "en-us"
            };
        }

        protected BaseAgent(Uri serviceAddress, string upnIdentity, string dataArea)
        {
            if (string.IsNullOrEmpty(upnIdentity))
                throw new ArgumentException("A UPN identity must be specified.");

            ServiceAddress = serviceAddress;

            UpnIdentity = upnIdentity;

            CallContext = new CallContext
            {
                Company = dataArea,
                Language = "en-us"
            };
        }

        /// <summary>
        /// Gets the service address.
        /// </summary>
        /// <value>
        /// The service address.
        /// </value>
        public Uri ServiceAddress { get; }

        /// <summary>
        /// Gets the upn identity.
        /// </summary>
        /// <value>
        /// The upn identity.
        /// </value>
        public string UpnIdentity { get; }

        /// <summary>
        /// Gets the call context.
        /// </summary>
        /// <value>
        /// The call context.
        /// </value>
        public CallContext CallContext { get; }
    }

}