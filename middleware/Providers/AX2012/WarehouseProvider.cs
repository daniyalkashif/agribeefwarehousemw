﻿using AgriBeef.WarehouseMW.Common;
using AgriBeef.WarehouseMW.Types;
using AgriBeef.WarehouseMW.WHSMobileValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Web;
using System.Text.RegularExpressions;

namespace AgriBeef.WarehouseMW.Providers.AX2012
{
    public class WarehouseProvider: BaseAgent, IWarehouseProvider
    {
        #region Constructor
        /// <summary>
        /// Initializes a new instance of a <see cref="WarehouseProvider"/> class.
        /// </summary>
        /// <param name="serviceAddress"></param>
        /// <param name="upnIdentity"></param>
        /// <param name="dataArea"></param>
        public WarehouseProvider(Uri serviceAddress, string upnIdentity, string dataArea)
            : base(serviceAddress, upnIdentity, dataArea)
        {
        }
        #endregion

        public SvcResult<WebResponse> CheckOrderNum(string orderNum)
        {
            SvcResult<WebResponse> svcWebResponseResult;
            WebResponse webResponse = new WebResponse();
            try
            {
                using (var client = ServiceClientFactory<WHSMobileValidation.abWHSMobileValidationServiceChannel>.GetClient(ServiceAddress, UpnIdentity))
                {
                    var request = new abWHSMobileValidationServiceCheckOrderNumRequest(CallContext, orderNum);
                    var results = client.checkOrderNum(request);

                    webResponse = results;

                    svcWebResponseResult = new SvcResult<WebResponse>(webResponse, true, "", null,ValidationType.Success);
                }
            }
            catch (FaultException<AifFault> ex)
            {
                var aifFaultMsg = ex.ExtractFaultMessages();
                svcWebResponseResult = new SvcResult<WebResponse>(new WebResponse() { Message = aifFaultMsg.Replace("\t", "").Replace("\r", "").Replace("\n", "") }, false, $"An error occurred in CheckOrderNum.{System.Environment.NewLine}{aifFaultMsg}", ex, ValidationType.Exception);
                Logger.LogException(svcWebResponseResult.Message);
            }
            catch (Exception ex)
            {
                svcWebResponseResult = new SvcResult<WebResponse>(new WebResponse() { Message = "An error occurred" }, false, $"An error occurred in CheckOrderNum.", ex, ValidationType.Exception);
                Logger.LogException(svcWebResponseResult.Message);
            }
            return svcWebResponseResult;
        }
        public SvcResult<WebResponse> CheckSerial(string orderNum, string serialId)
        {
            SvcResult<WebResponse> svcWebResponseResult;
            WebResponse webResponse = new WebResponse();
            try
            {
                using (var client = ServiceClientFactory<WHSMobileValidation.abWHSMobileValidationServiceChannel>.GetClient(ServiceAddress, UpnIdentity))
                {
                    var request = new abWHSMobileValidationServiceCheckSerialRequest(CallContext, orderNum, serialId);
                    var results = client.checkSerial(request);

                    webResponse = results;

                    svcWebResponseResult = new SvcResult<WebResponse>(webResponse, true, "", null, ValidationType.Success);
                }
            }
            catch (FaultException<AifFault> ex)
            {
                var aifFaultMsg = ex.ExtractFaultMessages();
                svcWebResponseResult = new SvcResult<WebResponse>(new WebResponse() { Message = aifFaultMsg.Replace("\t", "").Replace("\r", "").Replace("\n", "") }, false, $"An error occurred in CheckSerial.{System.Environment.NewLine}{aifFaultMsg}", ex, ValidationType.Exception);
                Logger.LogException(svcWebResponseResult.Message);
            }
            catch (Exception ex)
            {
                svcWebResponseResult = new SvcResult<WebResponse>(new WebResponse() { Message = "An error occured" }, false, $"An error occurred in CheckSerial.", ex, ValidationType.Exception);
                Logger.LogException(svcWebResponseResult.Message);
            }
            return svcWebResponseResult;
        }

        public SvcResult<WebResponse> CheckStageLP(string licensePlate)
        {
            SvcResult<WebResponse> svcWebResponseResult;
            WebResponse webResponse = new WebResponse();
            try
            {
                using (var client = ServiceClientFactory<WHSMobileValidation.abWHSMobileValidationServiceChannel>.GetClient(ServiceAddress, UpnIdentity))
                {
                    var request = new abWHSMobileValidationServiceCheckStageLPRequest(CallContext, licensePlate);
                    var results = client.checkStageLP(request);

                    webResponse = results;

                    svcWebResponseResult = new SvcResult<WebResponse>(webResponse, true, "", null, ValidationType.Success);
                }
            }
            catch (FaultException<AifFault> ex)
            {
                var aifFaultMsg = ex.ExtractFaultMessages();
                svcWebResponseResult = new SvcResult<WebResponse>(new WebResponse() { Message = aifFaultMsg.Replace("\t", "").Replace("\r", "").Replace("\n", "") }, false, $"An error occurred in CheckStageLP.{System.Environment.NewLine}{aifFaultMsg}", ex, ValidationType.Exception);
                Logger.LogException(svcWebResponseResult.Message);
            }
            catch (Exception ex)
            {
                svcWebResponseResult = new SvcResult<WebResponse>(new WebResponse() { Message = "An error occured" }, false, $"An error occurred in CheckStageLP.", ex, ValidationType.Exception);
                Logger.LogException(svcWebResponseResult.Message);
            }
            return svcWebResponseResult;
        }
        public SvcResult<WebResponse> PickSerial(PickSerialRequest request)
        {
            SvcResult<WebResponse> svcWebResponseResult;
            WebResponse webResponse = new WebResponse();
            try
            {
                using (var client = ServiceClientFactory<WHSMobileValidation.abWHSMobileValidationServiceChannel>.GetClient(ServiceAddress, UpnIdentity))
                {
                    var axRequest = new abWHSMobileValidationServicePickSerialRequest(CallContext, request.OrderNum, request.LicensePlateId, request.SerialId);
                    var results = client.pickSerial(axRequest);

                    webResponse = results;

                    svcWebResponseResult = new SvcResult<WebResponse>(webResponse, true, "", null, ValidationType.Success);
                }
            }
            catch (FaultException<AifFault> ex)
            {
                var aifFaultMsg = ex.ExtractFaultMessages();
                svcWebResponseResult = new SvcResult<WebResponse>(new WebResponse() { Message = aifFaultMsg.Replace("\t", "").Replace("\r", "").Replace("\n", "") }, false, $"An error occurred in PickSerial method.{System.Environment.NewLine}{aifFaultMsg}", ex, ValidationType.Exception);
                Logger.LogException(svcWebResponseResult.Message);
            }
            catch (Exception ex)
            {
                svcWebResponseResult = new SvcResult<WebResponse>(new WebResponse() { Message = "An error occured" }, false, $"An error occurred in PickSerial method.", ex, ValidationType.Exception);
                Logger.LogException(svcWebResponseResult.Message);
            }
            return svcWebResponseResult;
        }
    }
}