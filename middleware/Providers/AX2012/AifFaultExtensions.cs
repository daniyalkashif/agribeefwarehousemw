﻿using AgriBeef.WarehouseMW.WHSMobileValidation;
using System.ServiceModel;
using System.Text;

namespace AgriBeef.WarehouseMW.Providers.AX2012
{
    public static class AifFaultExtensions
    {
        /// <summary>
        /// Extracts the fault messages.
        /// </summary>
        /// <param name="aifFault">The aif fault.</param>
        /// <returns></returns>
        public static string ExtractFaultMessages(this FaultException<AifFault> aifFault)
        {
            var msg = new StringBuilder();
            if (aifFault?.Detail == null)
                return msg.ToString();

            if (aifFault.Detail.FaultMessageListArray != null)
            {
                foreach (var faultMsgList in aifFault.Detail.FaultMessageListArray)
                {
                    if (faultMsgList.FaultMessageArray == null)
                        continue;

                    foreach (var faultMsg in faultMsgList.FaultMessageArray)
                        msg.AppendLine($"{faultMsg.Message}");
                }
            }

            if (aifFault.Detail.InfologMessageList != null)
            {
                foreach (var infoMsg in aifFault.Detail.InfologMessageList)
                    msg.AppendLine($"{infoMsg.InfologMessageType}: {infoMsg.Message}");
            }

            return msg.ToString();
        }
    }

}